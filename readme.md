# user-crud-homework

## Installation

The project is composed of two parts:

### backend
The backend API is an express server that uses mongo-db for data storage.

Make sure that `serve/config.json` ponints to a valid mongo server and databse.

Look in `/server/API.md` for a description of the API endpoints.

Execute the following to run.
```
cd server
npm install -D
npm run build
npm start
```

### Frontend
This was developed using `react-create-app` with the `react-scripts-ts` scripts to use Typescript.

Right now, it is available through webpack's development server.

Connection to the backend is provided through Webpack's proxy option which expects the backend to be running at `localhost:3001`

Execute the following to run.
```
cd client
npm install -D
npm start
```

## Development notes.

### Frontend

The frontend is what I believe to be a bog standard "my-first-react-project" application.

It was written using Typescript and follows the standard pattern of extending React.Component to provide different views to render.

React-Router was used to provide routing in the application.

The backend provided JWT token is stored in local storage and an API class provides an extended `fetch` method that is used to send queries to the backend with the JWT token.


### Backend

The backend was written with ES6 modules and is compiled with `babel`.

All the API endpoints were developed using Test-Driven-Development and are under unit test.

I couldn't get my hands on a Postmark API key
(my only email address is with gmail and the want an address from a private server)
Therefore, the email sending functionality is somewhat untested.
The NPM module I am using does output sent emails into a tmp folder and renders them in your browser, however.

Each endpoint consists of three files:
  - `endpoint.factory.js`
    Contains the endpoint in a naive state where it has no dependencies on the rest of the application
  - `endpoint.test.js`
    A Jest unit test to check the factory function's work.
  - `endpoint.js`
    This injects actual code into the factory to create the endpoint that we attach to Express.
