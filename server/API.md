# Publicly available API endpoints

## POST /api/auth/login
Used to log into the application and receive a JWT token for further API queries.

Expects an JSON encoded object like:
```
    {
      email: 'bob@bob.bob',
      password: 'bobbobbob'
    }
```
Returns an JSON object with a JWT token:
```
  {
    token: "JWT.STRING.HERE"
  }
```
Returns `{error: "NO_USER"}` if there is no user with the provide email.

Returns `{error: "WRONG_PASSWORD"}` if the provided password does not match the user.

## POST /api/auth/register
Used to register a new user.

This will also send a validation email to the user's email address.

Expects an JSON encoded object like:
```
  {
    email: 'bob@bob.bob',
    password: 'bobbobbob'
  }
```
Returns `{status: 'success'}` if the user is added.

Returns `{error: "USER_EXISTS"}` if there is already an user with the provided email.

Returns `{error: "BAD_PASSWORD"}` if the password is shorter than 8 characters.

Returns `{error: "BAD_EMAIL"}` if the provided email does not pass the laxest of an email checking regular expression.

## POST /api/auth/reset
Resets an user's password and sends a password reminder email to the user.

Expects an JSON encoded object like:
```
  {
    email: 'bob@bob.bob',
  }
```

## GET /api/validate/:key
Validates an user based on the key that was emailed to them.

Returns a JWT token like `/api/login` so the validated user can start using the system.

Returns `{error: "INVALID_KEY"}` if no user with the given validation key exists.

# Privately available API endpoints
These are only accessible by providing a JWT token in a HTTP header:
```
Authorization: Bearer JWT.TOKEN.HERE
```

## GET /api/users/:email
Provides a JSON object containing details about the user with the input email.
```
{
  email: 'bob@bob.bob',
  logins: [
    // UNIX timestamps of the user's logins.
  ]
}
```

## GET /api/users/:skip/:count
Provides a list of users, skipping ```:skip``` many and returning up to ```:count```.

It will look like:
```
[
  { email: 'bob@bob.bob' },
  { email: 'rob@rob.rob' }
]
```
## DELETE /api/users/:email
Deletes an user from the system.

It will return ```{ error: "WONT_DELETE_SELF"}``` if attempting to delete the user associated with yout JWT token.

## POST /api/users/add
At the moment, this works exactly like ```/api/auth/register```

It does not require the added user to validate their email, however.
