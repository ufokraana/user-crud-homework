import getDB from './getDB';

const storeUser = (user) => {
    return getDB()
        .then(db => db.collection('users'))
        .then(collection => collection.insert(user));
};

export default storeUser;
