import getDB from './getDB';

const validateUserByKey = (pendingValidation) => {
    let col;
    return getDB()
        .then(db => db.collection('users'))
        .then((myCol) => {
            col = myCol;
        })
        .then(() => col.findOne({ pendingValidation }))
        .then((user) => {
            if (!user) {
                return Promise.reject();
            }
            return true;
        })
        .then(() => col.findOneAndUpdate(
            { pendingValidation },
            { $unset: { pendingValidation: '' } }
        ))
        .then(user => user.value)
        .catch(() => null);
};

export default validateUserByKey;
