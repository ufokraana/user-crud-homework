import getDB from './getDB';

const getUsers = (skip, limit) => {
    skip = Number(skip);
    limit = Number(limit);
    const out = {};
    return getDB()
        .then(db => db.collection('users'))
        .then(col => Promise.all([
            col.count().then((count) => {
                out.total = count;
            }),
            col.find()
                .project({ email: true })
                .skip(skip)
                .limit(limit)
                .toArray()
                .then((users) => {
                    out.users = users;
                })
        ]))
        .then(() => out);
};

export default getUsers;
