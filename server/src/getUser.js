import getDB from './getDB';

const getUser = (email) => {
    return getDB()
        .then(db => db.collection('users'))
        .then(collection => collection.findOne({ email }))
        .then((user) => {
            if (user) {
                return {
                    email: user.email,
                    password: user.password,
                    pendingValidation: user.pendingValidation
                };
            }
            return null;
        });
};

export default getUser;
