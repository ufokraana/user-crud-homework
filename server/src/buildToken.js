import { sign } from 'jsonwebtoken';
import config from './config';


const buildToken = payload => sign(payload, config.jwtSecret);

export default buildToken;
