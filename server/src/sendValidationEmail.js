import config from './config';
import sendEmail from './sendMail';

const sendValidationEmail = (address, key) => {
    const validationURL = `${config.frontendURL}/validate/${key}`;
    sendEmail(address, 'validation', { address, validationURL });
};

export default sendValidationEmail;
