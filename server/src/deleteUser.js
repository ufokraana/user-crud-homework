import getDB from './getDB';

const deleteUser = (email) => {
    return getDB()
        .then(db => db.collection('users'))
        .then(collection => collection.findOneAndDelete({ email }));
};

export default deleteUser;
