import { MongoClient } from 'mongodb';
import config from './config';

let client;
let database;

const buildDB = MongoClient.connect(config.mongoURL)
    .then((myClient) => {
        client = myClient;
        database = client.db(config.mongoDB);
        return database;
    });

const getDB = () => {
    if (database) {
        return Promise.resolve(database);
    }
    return buildDB();
};

export default getDB;
