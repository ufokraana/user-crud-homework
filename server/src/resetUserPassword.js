import getDB from './getDB';

const critters = ['Beetle', 'Caterpillar', 'Butterfly'];
const foods = ['Juice', 'Jam', 'Burger'];
const jargons = ['Exception', 'Factory', 'Manager'];

const getRandItem = (list) => {
    const index = Math.floor(Math.random() * list.length);
    return list[index];
};

const getNewPassword = () => {
    let password = '';

    password += getRandItem(critters);
    password += getRandItem(foods);
    password += getRandItem(jargons);

    return password;
};

const resetUserPassword = (email) => {
    const password = getNewPassword();
    return getDB()
        .then(db => db.collection('users'))
        .then(col => col.findOneAndUpdate({ email }, { $set: { password } }))
        .then(() => password);
};

export default resetUserPassword;
