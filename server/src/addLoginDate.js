import getDB from './getDB';

const addLoginDate = (email) => {
    return getDB()
        .then(db => db.collection('users'))
        .then(collection => (
            collection.updateOne(
                { email },
                {
                    $push: {
                        logins: Date.now()
                    }
                }
            )
        ));
};

export default addLoginDate;
