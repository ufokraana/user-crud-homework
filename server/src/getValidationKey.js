const getRandomDigit = () => {
    const digit = Math.floor(Math.random() * 10);
    return `${digit}`;
};

const getValidationKey = () => {
    let key = '';

    for (let i = 0; i < 20; i += 1) {
        key += getRandomDigit();
    }
    return key;
};

export default getValidationKey;
