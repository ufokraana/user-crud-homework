import Email from 'email-templates';

import nodemailer from 'nodemailer';
import postmarkTransport from 'nodemailer-postmark-transport';

import config from './config';

let transport;
if (config.postMarkToken) {
    transport = nodemailer.createTransport(postmarkTransport({
        auth: {
            apiKey: config.postMarkToken
        }
    }));
}
else {
    transport = {
        jsonTransport: true
    };
}

const email = new Email({
    message: {
        from: config.emailFrom
    },
    transport
});

const sendMail = (address, template, locals) => {
    return email.send({
        template,
        message: {
            to: address
        },
        locals
    });
};

export default sendMail;
