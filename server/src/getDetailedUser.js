import getDB from './getDB';

const getDetailedUser = (email) => {
    return getDB()
        .then(db => db.collection('users'))
        .then(collection => collection.findOne({ email }, { projection: { password: false } }));
};

export default getDetailedUser;
