import makeResetPassword from './resetPassword.factory';
import getUser from '../../getUser';
import resetUserPassword from '../../resetUserPassword';
import sendMail from '../../sendMail';

const sendPasswordResetEmail = (email, password) => {
    sendMail(email, 'reset-password', { password });
};

const resetPassword = makeResetPassword({
    getUser,
    resetUserPassword,
    sendPasswordResetEmail
});

export default resetPassword;
