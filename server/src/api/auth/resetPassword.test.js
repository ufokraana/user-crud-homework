import makeResetPassword from './resetPassword.factory';
// mocked express call signature
let req = null;
let res = null;
let next = null;
let body = null;
let params = null;

// This allows us to inspect function mocks after next() is called.
let nextPromise = null;
const setNext = () => {
    nextPromise = new Promise((resolve) => {
        next = () => resolve();
    });
};

beforeEach(() => {
    params = {};
    body = {};
    req = {
        body,
        params
    };
    res = {
        end: jest.fn().mockReturnThis(),
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis()
    };
    setNext();
});

describe('Express callback that', () => {
    it('resets an users password and sends them an email', () => {
        const deps = {
            getUser: jest.fn().mockResolvedValue({ email: 'bob@bob.bob' }),
            resetUserPassword: jest.fn().mockResolvedValue('hunter2'),
            sendPasswordResetEmail: jest.fn().mockResolvedValue()
        };
        const resetPassword = makeResetPassword(deps);

        body.email = 'bob@bob.bob';

        resetPassword(req, res, next);
        return nextPromise.then(() => {
            expect(deps.resetUserPassword.mock.calls[0][0]).toBe('bob@bob.bob');
            expect(deps.sendPasswordResetEmail.mock.calls[0][0]).toBe('bob@bob.bob', 'hunter2');
            expect(res.status.mock.calls[0][0]).toBe(200);
        });
    });
    it('does not send the email if there is no such user', () => {
        const deps = {
            getUser: jest.fn().mockResolvedValue(null),
            resetUserPassword: jest.fn().mockResolvedValue('hunter2'),
            sendPasswordResetEmail: jest.fn().mockResolvedValue()
        };
        const resetPassword = makeResetPassword(deps);

        body.email = 'bob@bob.bob';

        resetPassword(req, res, next);
        return nextPromise.then(() => {
            expect(deps.resetUserPassword.mock.calls.length).toBe(0);
            expect(deps.sendPasswordResetEmail.mock.calls.length).toBe(0);
            expect(res.status.mock.calls[0][0]).toBe(200);
        });
    });
    it('sends http status 500 if something else breaks.', () => {
        const deps = {
            getUser: jest.fn().mockRejectedValue()
        };
        const resetPassword = makeResetPassword(deps);

        body.email = 'bob@bob.bob';

        resetPassword(req, res, next);
        return nextPromise.then(() => {
            expect(res.status.mock.calls[0][0]).toBe(500);
        });
    });
});
