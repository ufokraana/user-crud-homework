const makeLogin = ({
    getUser,
    buildToken,
    addLoginDate
}) => (req, res, next) => {
    const { email, password } = req.body;
    let user;
    let error;
    getUser(email)
        .then((myUser) => {
            user = myUser;
            if (!user) {
                error = 'NO_USER';
                return Promise.reject();
            }
            else if (user.pendingValidation) {
                error = 'NOT_VALIDATED';
                return Promise.reject();
            }
            else if (user.password !== password) {
                error = 'WRONG_PASSWORD';
                return Promise.reject();
            }
            return true;
        })
        .then(() => addLoginDate(email))
        .then(() => buildToken({ email }))
        .then((token) => {
            res.json({ token });
            next();
        })
        .catch(() => {
            if (error) {
                res.json({ error });
            }
            else {
                res.status(500);
                res.end();
            }
            next();
        });
};

export default makeLogin;
