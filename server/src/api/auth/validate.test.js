import makeValidate from './validate.factory';

// mocked express call signature
let req = null;
let res = null;
let next = null;
let params = null;

// This allows us to inspect function mocks after next() is called.
let nextPromise = null;
const setNext = () => {
    nextPromise = new Promise((resolve) => {
        next = () => resolve();
    });
};

beforeEach(() => {
    params = {};
    req = {
        params
    };
    res = {
        end: jest.fn().mockReturnThis(),
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis()
    };
    setNext();
});

describe('Express middleware that', () => {
    it('validates an user', () => {
        const user = {
            email: 'bob@bob.bob'
        };
        const token = 'xxx.yyy.zzz';
        const deps = {
            validateUserByKey: jest.fn().mockResolvedValue(user),
            buildToken: jest.fn().mockReturnValue(token)
        };

        const validate = makeValidate(deps);
        params.key = '1234';

        validate(req, res, next);

        return nextPromise.then(() => {
            expect(deps.validateUserByKey.mock.calls[0][0]).toBe(params.key);
            expect(deps.buildToken.mock.calls[0][0].email).toBe('bob@bob.bob');
            expect(deps.buildToken.mock.calls[0][0].password).toBe(undefined);
            expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({
                token
            }));
        });
    });
    it('sends back {error: "INVALID_KEY"} if no such key exists', () => {
        const deps = {
            validateUserByKey: jest.fn().mockResolvedValue(null),
            buildToken: jest.fn().mockReturnValue()
        };

        const validate = makeValidate(deps);
        params.key = '1234';

        validate(req, res, next);

        return nextPromise.then(() => {
            expect(deps.validateUserByKey.mock.calls[0][0]).toBe(params.key);
            expect(deps.buildToken.mock.calls.length).toBe(0);
            expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({
                error: 'INVALID_KEY'
            }));
        });
    });
    it('sends back a 500 error if something else breaks', () => {
        const deps = {
            validateUserByKey: jest.fn().mockRejectedValue(null)
        };

        const validate = makeValidate(deps);
        params.key = '1234';

        validate(req, res, next);

        return nextPromise.then(() => {
            expect(res.status.mock.calls[0][0]).toBe(500);
        });
    });
});
