import makeLogin from './login.factory';

// mocked express call signature
let req = null;
let res = null;
let next = null;
let body = null;

// This allows us to inspect function mocks after next() is called.
let nextPromise = null;
const setNext = () => {
    nextPromise = new Promise((resolve) => {
        next = () => resolve();
    });
};
beforeEach(() => {
    body = {};
    req = {
        body
    };
    res = {
        end: jest.fn().mockReturnThis(),
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis()
    };
    setNext();
});
describe('provides an express callback which', () => {
    it('sends back a user token', () => {
        const email = 'bob@bob.bob';
        const user = {
            email,
            password: 'bob'
        };

        const getUser = jest.fn().mockResolvedValue(user);

        const token = 'xxx.yyy.zzz';
        const buildToken = jest.fn().mockReturnValue(token);

        const addLoginDate = jest.fn().mockResolvedValue();

        const login = makeLogin({
            getUser,
            buildToken,
            addLoginDate
        });

        body.email = 'bob@bob.bob';
        body.password = 'bob';

        login(req, res, next);

        return nextPromise.then(() => {
            expect(getUser.mock.calls[0][0]).toBe('bob@bob.bob');
            expect(buildToken.mock.calls[0][0].email).toBe('bob@bob.bob');
            expect(buildToken.mock.calls[0][0].password).toBe(undefined);
            expect(addLoginDate.mock.calls[0][0]).toBe('bob@bob.bob');
            expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({
                token
            }));
        });
    });
    it('sends back {error: "NOT_VALIDATED"} if the user has pending validation', () => {
        const getUser = jest.fn().mockResolvedValue({
            email: 'bob@bob.bob',
            password: 'bob',
            pendingValidation: 'yes'
        });

        const buildToken = jest.fn();

        const login = makeLogin({
            getUser,
            buildToken
        });

        body.email = 'bob@bob.bob';
        body.password = 'bob';

        login(req, res, next);

        return nextPromise.then(() => {
            expect(buildToken.mock.calls.length).toBe(0);
            expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({
                error: 'NOT_VALIDATED'
            }));
        });
    });
    it('sends back {error: "WRONG_PASSWORD"} if provided with a bogus password', () => {
        const getUser = jest.fn().mockResolvedValue({
            email: 'bob@bob.bob',
            password: 'notbob'
        });

        const buildToken = jest.fn();

        const login = makeLogin({
            getUser,
            buildToken
        });

        body.email = 'bob@bob.bob';
        body.password = 'bob';

        login(req, res, next);

        return nextPromise.then(() => {
            expect(buildToken.mock.calls.length).toBe(0);
            expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({
                error: 'WRONG_PASSWORD'
            }));
        });
    });
    it('sends back {error: "NO_USER"}, if no user is found', () => {
        const getUser = jest.fn().mockResolvedValue(null);

        const login = makeLogin({ getUser });

        body.email = undefined;
        body.password = undefined;

        login(req, res, next);

        return nextPromise.then(() => {
            expect(getUser.mock.calls.length).toBe(1);
            expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({
                error: 'NO_USER'
            }));
        });
    });
    it('sends back a 500 error if getUser rejects.', () => {
        const login = makeLogin({
            getUser: jest.fn().mockRejectedValue()
        });

        login(req, res, next);

        return nextPromise.then(() => {
            expect(res.status.mock.calls[0][0]).toBe(500);
            expect(res.end.mock.calls.length).toBe(1);
        });
    });
});
