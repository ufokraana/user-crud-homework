const makeResetPassword = ({
    getUser,
    resetUserPassword,
    sendPasswordResetEmail
}) => (req, res, next) => {
    const { email } = req.body;
    let fakeSuccess = false;
    Promise.resolve()
        .then(() => getUser(email))
        .then((user) => {
            if (user) {
                return true;
            }
            fakeSuccess = true;
            return Promise.reject();
        })
        .then(() => resetUserPassword(email))
        .then(password => sendPasswordResetEmail(email, password))
        .then(() => res.status(200))
        .catch(() => {
            if (fakeSuccess) {
                res.status(200);
            }
            else {
                res.status(500);
            }
        })
        .then(next);
};

export default makeResetPassword;
