import makeLogin from './login.factory';
import buildToken from '../../buildToken';
import getUser from '../../getUser';
import addLoginDate from '../../addLoginDate';

/*
const fakeGetUser = (email) => {
    if (email === 'bob@bob.bob') {
        return Promise.resolve({ email: 'bob@bob.bob', password: 'bob' });
    }
    return Promise.reject();
};
*/

const login = makeLogin({
    getUser,
    buildToken,
    addLoginDate
});

export default login;
