import makeRegister from './register.factory';
import getUser from '../../getUser';
import storeUser from '../../storeUser';
import sendValidationEmail from '../../sendValidationEmail';
import getValidationKey from '../../getValidationKey';

const isValidAddress = (email) => {
    // this tests for something@something.something
    // Doing anything more would be madness.
    const rule = /.+@.+\..+/;
    return rule.test(email);
};


const register = makeRegister({
    getUser,
    isValidAddress,
    getValidationKey,
    sendValidationEmail,
    storeUser
});


export default register;
