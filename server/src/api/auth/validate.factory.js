const makeValidate = ({
    validateUserByKey,
    buildToken
}) => (req, res, next) => {
    let error;

    Promise.resolve()
        .then(() => validateUserByKey(req.params.key))
        .then((user) => {
            if (!user) {
                error = 'INVALID_KEY';
                return Promise.reject();
            }
            return buildToken({ email: user.email });
        })
        .then(token => res.json({ token }))
        .catch(() => {
            if (error) {
                res.json({ error });
            }
            else {
                res.status(500);
            }
        })
        .then(next);
};

export default makeValidate;
