import makeRegister from './register.factory';

// mocked express call signature
let req = null;
let res = null;
let next = null;
let body = null;

// This allows us to inspect function mocks after next() is called.
let nextPromise = null;
const setNext = () => {
    nextPromise = new Promise((resolve) => {
        next = () => resolve();
    });
};

beforeEach(() => {
    body = {};
    req = {
        body
    };
    res = {
        end: jest.fn().mockReturnThis(),
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis()
    };
    setNext();
});

describe('makeRegister({getUser, isValidAddress, sendValidationEmail, storeUser})', () => {
    describe('builds an express callback that', () => {
        it('makes a new user record and sends a validation email.', () => {
            const pendingValidation = 'asdfasdf';
            const deps = {
                getUser: jest.fn().mockResolvedValue(undefined),
                getValidationKey: jest.fn().mockResolvedValue(pendingValidation),
                isValidAddress: jest.fn().mockReturnValue(true),
                sendValidationEmail: jest.fn().mockResolvedValue(true),
                storeUser: jest.fn().mockResolvedValue(true)
            };
            const register = makeRegister(deps);

            const email = 'bob@bob.bob';
            const password = 'bobbobbob';
            body.email = email;
            body.password = password;

            register(req, res, next);


            return nextPromise.then(() => {
                expect(deps.getUser.mock.calls[0][0]).toBe(email);
                expect(deps.isValidAddress.mock.calls[0][0]).toBe(email);
                expect(deps.sendValidationEmail.mock.calls[0]).toEqual((
                    expect.arrayContaining(['bob@bob.bob', pendingValidation])
                ));
                expect(deps.storeUser.mock.calls[0][0]).toEqual(expect.objectContaining({ email, password, pendingValidation }));

                expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({ status: 'SUCCESS' }));
            });
        });
        it('sends back {error:"USER_EXISTS"} if an user already exits', () => {
            const register = makeRegister({
                getUser: jest.fn().mockResolvedValue({ email: 'bob@bob.bob' }),
                getValidationKey: jest.fn().mockResolvedValue('123')
            });

            body.email = 'bob@bob.bob';
            body.password = 'bobbobbob';
            register(req, res, next);

            return nextPromise.then(() => {
                expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({ error: 'USER_EXISTS' }));
            });
        });
        it('sends back {error:"BAD_EMAIL"} if the provided email is bogus', () => {
            const register = makeRegister({
                getUser: jest.fn().mockResolvedValue(undefined),
                isValidAddress: jest.fn().mockReturnValue(false),
                getValidationKey: jest.fn().mockResolvedValue('123')
            });

            body.email = 'bob@bob.bob';
            body.password = 'bobbobbob';
            register(req, res, next);

            return nextPromise.then(() => {
                expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({ error: 'BAD_EMAIL' }));
            });
        });
        it('sends back {error:"BAD_PASSWORD"} if the provided password is shorter than 8', () => {
            const register = makeRegister({
                getUser: jest.fn().mockResolvedValue(undefined),
                isValidAddress: jest.fn().mockReturnValue(true),
                getValidationKey: jest.fn().mockResolvedValue('123')
            });

            body.email = 'bob@bob.bob';
            body.password = 'bob';
            register(req, res, next);

            return nextPromise.then(() => {
                expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({ error: 'BAD_PASSWORD' }));
            });
        });
        it('sends back status code 500 if it fails to send the validation email', () => {
            const deps = {
                getUser: jest.fn().mockResolvedValue(undefined),
                isValidAddress: jest.fn().mockReturnValue(true),
                sendValidationEmail: jest.fn().mockRejectedValue(),
                getValidationKey: jest.fn().mockResolvedValue('123')
            };
            const register = makeRegister(deps);

            const email = 'bob@bob.bob';
            const password = 'bobbobbob';
            body.email = email;
            body.password = password;

            register(req, res, next);


            return nextPromise.then(() => {
                expect(deps.getUser.mock.calls[0][0]).toBe(email);
                expect(deps.isValidAddress.mock.calls[0][0]).toBe(email);
                expect(deps.sendValidationEmail.mock.calls[0][0]).toBe(email);

                expect(res.status.mock.calls[0][0]).toBe(500);
                expect(res.end.mock.calls.length).toBe(1);
            });
        });
        it('sends back status code 500 if it fails to store the user', () => {
            const deps = {
                getUser: jest.fn().mockResolvedValue(undefined),
                isValidAddress: jest.fn().mockReturnValue(true),
                sendValidationEmail: jest.fn().mockResolvedValue(),
                storeUser: jest.fn().mockRejectedValue(),
                getValidationKey: jest.fn().mockResolvedValue('123')
            };
            const register = makeRegister(deps);

            const email = 'bob@bob.bob';
            const password = 'bobbobbob';
            body.email = email;
            body.password = password;

            register(req, res, next);


            return nextPromise.then(() => {
                expect(deps.getUser.mock.calls[0][0]).toBe(email);
                expect(deps.isValidAddress.mock.calls[0][0]).toBe(email);
                expect(deps.sendValidationEmail.mock.calls[0][0]).toBe(email);

                expect(res.status.mock.calls[0][0]).toBe(500);
                expect(res.end.mock.calls.length).toBe(1);
            });
        });
    });
});
