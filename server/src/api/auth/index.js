import express from 'express';
import login from './login';
import register from './register';
import resetPassword from './resetPassword';
import validate from './validate';

// eslint-disable-next-line
const auth = express.Router();

auth.post('/login', login);
auth.post('/register', register);
auth.post('/reset', resetPassword);
auth.get('/validate/:key', validate);
export default auth;
