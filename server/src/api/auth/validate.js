import makeValidate from './validate.factory';
import buildToken from '../../buildToken';
import validateUserByKey from '../../validateUserByKey';

const validate = makeValidate({
    validateUserByKey,
    buildToken
});

export default validate;
