const makeRegister = ({
    getUser,
    isValidAddress,
    sendValidationEmail,
    storeUser,
    getValidationKey
}) => (req, res, next) => {
    const { email, password } = req.body;
    let pendingValidation;

    // If the promise chain rejects,
    // this will be sent as {error: error}
    // If undefined, will trigger a HTTP 500 status code instead.
    let error;

    getUser(email)
        .then((user) => {
            if (user) {
                error = 'USER_EXISTS';
                return Promise.reject();
            }
            return true;
        })
        .then(() => {
            if (!password || password.length < 8) {
                error = 'BAD_PASSWORD';
                return Promise.reject();
            }
            return true;
        })
        .then(() => isValidAddress(email))
        .then((valid) => {
            if (!valid) {
                error = 'BAD_EMAIL';
                return Promise.reject();
            }
            return true;
        })
        .then(() => getValidationKey())
        .then((myKey) => {
            pendingValidation = myKey;
        })
        .then(() => sendValidationEmail(email, pendingValidation))
        .then(() => {
            const newUser = {
                email,
                password,
                pendingValidation
            };
            return storeUser(newUser);
        })
        .then(() => {
            res.json({ status: 'SUCCESS' });
            next();
        })
        .catch(() => {
            if (error) {
                res.json({ error });
            }
            else {
                res.status(500);
                res.end();
            }
            next();
        });
};

export default makeRegister;
