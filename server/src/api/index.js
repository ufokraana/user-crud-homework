import express from 'express';

import userRouter from './user/';
import authRouter from './auth/';

// eslint-disable-next-line
const APIRouter = express.Router();

APIRouter.use(express.json());
APIRouter.use('/auth', authRouter);
APIRouter.use('/users', userRouter);

export default APIRouter;
