const makeList = ({
    getUsers
}) => (req, res, next) => {
    let { skip, count } = req.params;
    if (Number.isNaN(+skip) || skip < 0) {
        skip = 0;
    }
    if (Number.isNaN(+count) || count < 0) {
        count = 5;
    }
    getUsers(skip, count)
        .then(users => res.json(users))
        .catch(() => res.status(500))
        .then(next);
};

export default makeList;
