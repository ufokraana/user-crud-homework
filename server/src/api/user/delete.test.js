import makeDelete from './delete.factory';

// mocked express call signature
let req = null;
let res = null;
let next = null;
let body = null;
let params = null;

// This allows us to inspect function mocks after next() is called.
let nextPromise = null;
const setNext = () => {
    nextPromise = new Promise((resolve) => {
        next = () => resolve();
    });
};

beforeEach(() => {
    params = {};
    body = {};
    req = {
        body,
        params
    };
    res = {
        end: jest.fn().mockReturnThis(),
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis()
    };
    setNext();
});

describe('Express callback that', () => {
    it('deletes an user', () => {
        const deps = {
            deleteUser: jest.fn().mockResolvedValue(true),
            getUser: jest.fn().mockResolvedValue({ email: 'bob@bob.bob' })
        };
        const del = makeDelete(deps);
        req.user = { email: 'rob@rob.rob' };
        params.id = 'bob@bob.bob';
        del(req, res, next);

        return nextPromise.then(() => {
            expect(deps.deleteUser.mock.calls[0][0]).toBe('bob@bob.bob');
            expect(res.status.mock.calls[0][0]).toBe(200);
        });
    });
    it('sends error code 403 if deleting token owner.', () => {
        const deps = {
            getUser: jest.fn().mockResolvedValue({ email: 'bob@bob.bob' })
        };
        const del = makeDelete(deps);
        req.user = { email: 'bob@bob.bob' };
        params.id = 'bob@bob.bob';
        del(req, res, next);

        return nextPromise.then(() => {
            expect(res.status.mock.calls[0][0]).toBe(403);
        });
    });
    it('sends http status 500 if something goes wrong', () => {
        const deps = {
            deleteUser: jest.fn().mockRejectedValue(),
            getUser: jest.fn().mockResolvedValue({ email: 'rob@rob.rob' })
        };
        const del = makeDelete(deps);

        req.user = { email: 'bob@bob.bob' };
        params.id = 'bob@bob.bob';
        del(req, res, next);

        return nextPromise.then(() => {
            expect(deps.deleteUser.mock.calls[0][0]).toBe('bob@bob.bob');
            expect(res.status.mock.calls[0][0]).toBe(500);
        });
    });
});
