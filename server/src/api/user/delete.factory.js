const makeDelete = ({
    deleteUser,
    getUser
}) => (req, res, next) => {
    let error;
    getUser(req.params.id)
        .then((user) => {
            if (user.email === req.user.email) {
                error = 'WONT_DELETE_SELF';
                return Promise.reject();
            }
            return true;
        })
        .then(() => deleteUser(req.params.id))
        .then(() => res.status(200))
        .catch(() => {
            if (error === 'WONT_DELETE_SELF') {
                res.status(403);
            }
            else {
                res.status(500);
            }
        })
        .then(next);
};

export default makeDelete;
