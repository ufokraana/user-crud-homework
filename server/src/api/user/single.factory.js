const makeSingle = ({
    getDetaileduser
}) => (req, res, next) => {
    const { id } = req.params;
    let user;
    let error;

    getDetaileduser(id)
        .then((myUser) => {
            user = myUser;
            if (!user) {
                error = 'NO_USER';
                return Promise.reject();
            }
            return true;
        })
        .then(() => res.json(user))
        .catch(() => {
            if (error) {
                res.json({ error });
            }
            else {
                res.status(500);
                res.end();
            }
        })
        .then(next);
};

export default makeSingle;
