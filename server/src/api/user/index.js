import express from 'express';
import jwt from 'express-jwt';
import config from '../../config';

import list from './list';
import single from './single';
import del from './delete';
import add from './add';

// eslint-disable-next-line
const userAPI = express.Router();

userAPI.use(jwt({ secret: config.jwtSecret }));

userAPI.get('/:id$', single);
userAPI.get('/:skip/:count$', list);
userAPI.delete('/:id$', del);
userAPI.post('/add', add);

export default userAPI;
