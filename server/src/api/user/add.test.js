import makeAdd from './add.factory';

// mocked express call signature
let req = null;
let res = null;
let next = null;
let body = null;

// This allows us to inspect function mocks after next() is called.
let nextPromise = null;
const setNext = () => {
    nextPromise = new Promise((resolve) => {
        next = () => resolve();
    });
};

beforeEach(() => {
    body = {};
    req = {
        body
    };
    res = {
        end: jest.fn().mockReturnThis(),
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis()
    };
    setNext();
});

describe('builds an express callback that', () => {
    it('makes a new user record and sends a validation email.', () => {
        const deps = {
            getUser: jest.fn().mockResolvedValue(undefined),
            isValidAddress: jest.fn().mockReturnValue(true),
            sendInvitationEmail: jest.fn().mockResolvedValue(true),
            storeUser: jest.fn().mockResolvedValue(true)
        };
        const register = makeAdd(deps);

        const email = 'bob@bob.bob';
        const password = 'bobbobbob';
        body.email = email;
        body.password = password;

        register(req, res, next);


        return nextPromise.then(() => {
            expect(deps.getUser.mock.calls[0][0]).toBe(email);
            expect(deps.isValidAddress.mock.calls[0][0]).toBe(email);
            expect(deps.sendInvitationEmail.mock.calls[0][0]).toBe(email);
            expect(deps.storeUser.mock.calls[0][0]).toEqual(expect.objectContaining({ email, password }));

            expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({ status: 'SUCCESS' }));
        });
    });
    it('sends back {error:"USER_EXISTS"} if an user already exits', () => {
        const register = makeAdd({
            getUser: jest.fn().mockResolvedValue({ email: 'bob@bob.bob' })
        });

        body.email = 'bob@bob.bob';
        body.password = 'bobbobbob';
        register(req, res, next);

        return nextPromise.then(() => {
            expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({ error: 'USER_EXISTS' }));
        });
    });
    it('sends back {error:"BAD_EMAIL"} if the provided email is bogus', () => {
        const register = makeAdd({
            getUser: jest.fn().mockResolvedValue(undefined),
            isValidAddress: jest.fn().mockReturnValue(false)
        });

        body.email = 'bob@bob.bob';
        body.password = 'bobbobbob';
        register(req, res, next);

        return nextPromise.then(() => {
            expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({ error: 'BAD_EMAIL' }));
        });
    });
    it('sends back {error:"BAD_PASSWORD"} if the provided password is shorter than 8', () => {
        const register = makeAdd({
            getUser: jest.fn().mockResolvedValue(undefined),
            isValidAddress: jest.fn().mockReturnValue(true)
        });

        body.email = 'bob@bob.bob';
        body.password = 'bob';
        register(req, res, next);

        return nextPromise.then(() => {
            expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining({ error: 'BAD_PASSWORD' }));
        });
    });
    it('sends back status code 500 if it fails to send the validation email', () => {
        const deps = {
            getUser: jest.fn().mockResolvedValue(undefined),
            isValidAddress: jest.fn().mockReturnValue(true),
            sendInvitationEmail: jest.fn().mockRejectedValue()
        };
        const register = makeAdd(deps);

        const email = 'bob@bob.bob';
        const password = 'bobbobbob';
        body.email = email;
        body.password = password;

        register(req, res, next);


        return nextPromise.then(() => {
            expect(deps.getUser.mock.calls[0][0]).toBe(email);
            expect(deps.isValidAddress.mock.calls[0][0]).toBe(email);
            expect(deps.sendInvitationEmail.mock.calls[0][0]).toBe(email);

            expect(res.status.mock.calls[0][0]).toBe(500);
            expect(res.end.mock.calls.length).toBe(1);
        });
    });
    it('sends back status code 500 if it fails to store the user', () => {
        const deps = {
            getUser: jest.fn().mockResolvedValue(undefined),
            isValidAddress: jest.fn().mockReturnValue(true),
            sendInvitationEmail: jest.fn().mockResolvedValue(),
            storeUser: jest.fn().mockRejectedValue()
        };
        const register = makeAdd(deps);

        const email = 'bob@bob.bob';
        const password = 'bobbobbob';
        body.email = email;
        body.password = password;

        register(req, res, next);


        return nextPromise.then(() => {
            expect(deps.getUser.mock.calls[0][0]).toBe(email);
            expect(deps.isValidAddress.mock.calls[0][0]).toBe(email);
            expect(deps.sendInvitationEmail.mock.calls[0][0]).toBe(email);

            expect(res.status.mock.calls[0][0]).toBe(500);
            expect(res.end.mock.calls.length).toBe(1);
        });
    });
});
