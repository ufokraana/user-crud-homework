import makeDelete from './delete.factory';

import deleteUser from '../../deleteUser';
import getUser from '../../getUser';

const del = makeDelete({
    deleteUser,
    getUser
});

export default del;
