import makeList from './list.factory';

// mocked express call signature
let req = null;
let res = null;
let next = null;
let params = null;

// This allows us to inspect function mocks after next() is called.
let nextPromise = null;
const setNext = () => {
    nextPromise = new Promise((resolve) => {
        next = () => resolve();
    });
};

beforeEach(() => {
    params = {};
    req = {
        params
    };
    res = {
        end: jest.fn().mockReturnThis(),
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis()
    };
    setNext();
});

describe('Express callback that', () => {
    it('provides a list of users in a json object', () => {
        const users = ['bob', 'rob', 'deb'];
        const deps = {
            getUsers: jest.fn().mockResolvedValue(users)
        };
        const list = makeList(deps);

        params.skip = 5;
        params.count = 10;
        list(req, res, next);

        return nextPromise.then(() => {
            expect(deps.getUsers.mock.calls[0]).toEqual((
                expect.arrayContaining([5, 10])
            ));
            expect(res.json.mock.calls[0][0]).toEqual((
                expect.objectContaining(users)
            ));
        });
    });
    it('defaults to skip = 0 and count = 5 if provided NaN or less than 0 input', () => {
        const users = ['bob', 'rob', 'deb'];
        const deps = {
            getUsers: jest.fn().mockResolvedValue(users)
        };
        const list = makeList(deps);

        params.skip = '-5';
        params.count = 'karuelabmetsas';
        list(req, res, next);

        return nextPromise.then(() => {
            expect(deps.getUsers.mock.calls[0]).toEqual((
                expect.arrayContaining([0, 5])
            ));
            expect(res.json.mock.calls[0][0]).toEqual((
                expect.objectContaining(users)
            ));
        });
    });
    it('returns http code 500 if something goes wrong.', () => {
        const deps = {
            getUsers: jest.fn().mockRejectedValue()
        };
        const list = makeList(deps);

        params.skip = 5;
        params.count = 10;
        list(req, res, next);

        return nextPromise.then(() => {
            expect(deps.getUsers.mock.calls[0]).toEqual((
                expect.arrayContaining([5, 10])
            ));
            expect(res.status.mock.calls[0][0]).toBe(500);
        });
    });
});
