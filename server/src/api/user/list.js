import makeList from './list.factory';
import getUsers from '../../getUsers';

/*
const fakeGetUsers = () => Promise.resolve({
    count: 3,
    users: [
        { email: 'bob@bob.bob' },
        { email: 'rob@rob.rob' },
        { email: 'dev@deb.deb' }
    ]
});
*/

const list = makeList({
    getUsers
});


export default list;
