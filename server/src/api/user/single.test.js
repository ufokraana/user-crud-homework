import makeSingle from './single.factory';

// mocked express call signature
let req = null;
let res = null;
let next = null;
let body = null;
let params = null;

// This allows us to inspect function mocks after next() is called.
let nextPromise = null;
const setNext = () => {
    nextPromise = new Promise((resolve) => {
        next = () => resolve();
    });
};

beforeEach(() => {
    params = {};
    body = {};
    req = {
        body,
        params
    };
    res = {
        end: jest.fn().mockReturnThis(),
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis()
    };
    setNext();
});

describe('Express callback that', () => {
    it('provides a single user\'s info', () => {
        const id = 'userid';
        const user = {
            _id: id,
            email: 'bob@bob.bob',
            logins: [
                123,
                456,
                789
            ]
        };
        const deps = {
            getDetaileduser: jest.fn().mockResolvedValue(user)
        };

        const single = makeSingle(deps);

        params.id = id;
        single(req, res, next);
        return nextPromise.then(() => {
            expect(deps.getDetaileduser.mock.calls[0][0]).toBe(id);
            expect(res.json.mock.calls[0][0]).toEqual(expect.objectContaining(user));
        });
    });
    it('sends {error: "NO_USER", if the user is not found}', () => {
        const deps = {
            getDetaileduser: jest.fn().mockResolvedValue(null)
        };

        const single = makeSingle(deps);

        params.id = 'something';
        single(req, res, next);
        return nextPromise.then(() => {
            expect(deps.getDetaileduser.mock.calls[0][0]).toBe(params.id);
            expect(res.json.mock.calls[0][0]).toEqual((
                expect.objectContaining({ error: 'NO_USER' })
            ));
        });
    });
    it('sends http status 500 if something else fails', () => {
        const deps = {
            getDetaileduser: jest.fn().mockRejectedValue(null)
        };

        const single = makeSingle(deps);

        params.id = 'something';
        single(req, res, next);
        return nextPromise.then(() => {
            expect(deps.getDetaileduser.mock.calls[0][0]).toBe(params.id);
            expect(res.status.mock.calls[0][0]).toBe(500);
            expect(res.end.mock.calls.length).toBe(1);
        });
    });
});
