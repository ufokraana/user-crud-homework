import makeRegister from './add.factory';
import getUser from '../../getUser';
import storeUser from '../../storeUser';
import sendEmail from '../../sendMail';

const isValidAddress = (email) => {
    // this tests for something@something.something
    // Doing anything more would be madness.
    const rule = /.+@.+\..+/;
    return rule.test(email);
};

const sendInvitationEmail = address => (
    sendEmail(address, 'invitation', { address })
);

const add = makeRegister({
    getUser,
    isValidAddress,
    sendInvitationEmail,
    storeUser
});


export default add;
