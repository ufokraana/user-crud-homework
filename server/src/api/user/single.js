import makeSingle from './single.factory';
import getDetaileduser from '../../getDetailedUser';

const single = makeSingle({ getDetaileduser });

export default single;
