import express from 'express';

import API from './api';

const app = express();

app.use('/api', API);

app.listen(3001, () => console.log('Server listening on port 3001'));
