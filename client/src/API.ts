import * as EventEmitter from 'events';
import { decode } from 'jsonwebtoken';

class API {
  public events = new EventEmitter;

  public token?:string;
  public user?:{[key:string]: any};

  public onToken (token:string) {
    const payload = decode(token);

    if (payload instanceof Object) {
      this.user = payload as {[key:string]: any};
      this.token = token;

      this.saveToStorage();

      this.events.emit('userAuth', this.user);
    }
  }

  public forgetToken () {
    this.user = undefined;
    this.token = undefined;
    localStorage.removeItem('userToken');
    this.events.emit('userLogout');
  }
  public fetchWithToken(url:string, opts:object = {}, headers:object = {}) {
    const optsWithToken = {
      headers: {
        Authorization: `Bearer ${this.token}`,
        ...headers,
      },
      ...opts,
    };
    return fetch(url, optsWithToken);
  }

  public loadFromStorage () {
    const token = localStorage.getItem('userToken');

    if (token) {
      this.onToken(token);
    }

    return this.user;
  }
  private saveToStorage () {
    localStorage.setItem('userToken', this.token as string);
  }
}

const myAPI = new API();
export default myAPI;
