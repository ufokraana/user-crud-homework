import * as React from 'react';
import {
  BrowserRouter as Router,
  Link,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';

import API from './API';
import AddUser from './components/AddUser';
import User from './components/User';
import Users from './components/Users';

export default class PrivateRouter extends React.Component {
  private static linkStyle = {
    border: '1px solid black',
    color: 'black',
    cursor: 'pointer',
    display: 'inline-block',
    margin: '5px 40px',
    padding: '4px',
    textDecoration: 'none',
  };
  public render() {
    const logoutOnClick = () => this.logOut();
    return (
      <div>
        <Router>
          <div>
            <a style={PrivateRouter.linkStyle} onClick={logoutOnClick}>Log out</a>
            <Link style={PrivateRouter.linkStyle} to="/">User List</Link>
            <Link style={PrivateRouter.linkStyle} to="/users/add">Add an User</Link>
              <Switch>
                <Route path="/users/add" component={AddUser}/>
                <Route path="/users/" component={Users}/>
                <Route path="/user/:id" component={User}/>
                <Route path="/validate">
                  <div>Cannot validate when logged in already.</div>
                </Route>
                <Redirect from="/" to="/users"/>
              </Switch>
          </div>
        </Router>
      </div>
    );
  }

  private logOut () {
    API.forgetToken();
  }
}
