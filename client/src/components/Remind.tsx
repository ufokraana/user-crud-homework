import * as React from 'react';

interface IState {
  email: string;
}

export default class Remind extends React.Component <any, IState>{
  public render() {
    const boundHandleChange = this.handleChange.bind(this);
    const boundOnLoginClicked = this.onLoginClicked.bind(this);

    return (
      <div>
      <h2>Reset your password here</h2>
        <label>Email:</label>
        <input
          type="email"
          name="email"
          onChange={boundHandleChange}
        />
        <br />
        <button onClick={boundOnLoginClicked}>Submit!</button>
      </div >
    );
  }
  private onLoginClicked (e:React.MouseEvent<HTMLButtonElement>) {
    fetch(
    '/api/auth/reset', {
      body: JSON.stringify({
        email: this.state.email,
      }),
      headers: {
        'content-type': 'application/json',
      },
      method: 'POST',
    },
    )
    .then(
      res => res.json(),
    );
  }
  private handleChange(e:React.ChangeEvent<HTMLInputElement>) {
    const name = e.target.name;
    const delta = {};
    delta[name] = e.target.value;
    this.setState(delta);
  }

}
