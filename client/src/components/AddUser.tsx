import * as React from 'react';
import { Redirect } from 'react-router-dom';

import API from '../API';

interface IState {
  email: string;
  password: string;
  error: string;
  done: boolean;
}

export default class AddUser extends React.Component <{}, IState>{
  constructor (props: {}) {
    super(props);
    this.state = {
      done: false,
      email: '',
      error: '',
      password: '',
    };
  }
  public render() {
    const boundHandleChange = this.handleChange.bind(this);
    const boundOnAddClicked = this.onAddClicked.bind(this);

    if (this.state.done) {
      return <Redirect to={'/user/' + this.state.email}/>;
    }
    return (
      <div>
        <h2>Add a new user</h2>
          {this.renderError()}
          <label>Email:</label>
          <input
            type="email"
            name="email"
            onChange={boundHandleChange}
          />
          <br />
          <label>Password:</label>
          <input
            type="password"
            name="password"
            onChange={boundHandleChange}
          />
          <br />
          <button onClick={boundOnAddClicked}>Submit!</button>
      </div>
    );
  }
  private renderError() {
    if (this.state.error) {
      return (
        <div className="Login-Error">
          {this.state.error}
        </div>
      );
    }
    return;
  }
  private onAddClicked (e:React.MouseEvent<HTMLButtonElement>) {
    API.fetchWithToken(
      '/api/users/add',
      {
        body: JSON.stringify({
          email: this.state.email,
          password: this.state.password,
        }),
        method: 'POST',
      },
      {
        'content-type': 'application/json',
      },
  )
    .then(
      res => res.json(),
    )
    .then(
      (json) => {
        if (json.error) {
          this.setState({ error: json.error });
        } else {
          this.setState({ done: true });
        }
      },
    );
  }
  private handleChange(e:React.ChangeEvent<HTMLInputElement>) {
    const name = e.target.name;
    const delta = {};
    delta[name] = e.target.value;
    this.setState(delta);
  }

}
