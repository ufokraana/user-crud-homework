import * as React from 'react';
import { Link } from 'react-router-dom';

import API from '../API';

interface IUser {
  _id: string;
  email: string;
}
interface IUserData {
  total: number;
  users: [IUser];
}
interface IState {
  userData?: IUserData;
  skip: number;
}

export default class Users extends React.Component <{}, IState> {
  public static usersPerPage = 2;

  constructor(props:{}) {
    super(props);

    this.state = {
      skip: 0,
    };

    this.fetchUsers(0);
  }

  public render() {
    if (this.state.userData) {
      const { users, total } = this.state.userData;
      const showNext = this.state.skip + Users.usersPerPage < total ;
      const showPrev = this.state.skip > 0;

      const onNext = this.onPagination.bind(this, Users.usersPerPage);
      const onPrev = this.onPagination.bind(this, -Users.usersPerPage);

      return (
      <div>
        <h2>The most magnaminous users:</h2>
        <ul>
          {users.map(this.renderUser)}
        </ul>
          <button onClick={onPrev} disabled={!showPrev}>Previous</button>
          <button onClick={onNext} disabled={!showNext}>Next</button>
      </div>
      );
    }
    return <div>Loading...</div>;
  }

  private onPagination (delta: number) {
    const newSkip = Math.max(this.state.skip + delta, 0);
    this.setState({ skip: newSkip, userData: undefined });
    this.fetchUsers(newSkip);
  }
  private renderUser (user:IUser) {
    return (
      <div>
        <li key={user._id}>
          <Link to={'/user/' + user.email}>
            {user.email}
          </Link>
        </li>
      </div>
    );
  }

  private fetchUsers (skip: number) {
    API.fetchWithToken(
      `/api/users/${skip}/${Users.usersPerPage}`,
    ).then(
      res => res.json(),
    ).then(
      this.onUsers.bind(this),
    );
  }

  private onUsers(userData:IUserData) {
    this.setState({ userData });
  }
}
