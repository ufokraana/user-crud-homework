import * as React from 'react';
import { Redirect } from 'react-router-dom';

interface IState {
  email: string;
  password: string;
  error: string;
  done: boolean;
}

export default class Register extends React.Component <{}, IState>{
  constructor (props: {}) {
    super(props);
    this.state = {
      done: false,
      email: '',
      error: '',
      password: '',
    };
  }
  public render() {
    const boundHandleChange = this.handleChange.bind(this);
    const boundOnLoginClicked = this.onRegisterClicked.bind(this);

    if (this.state.done) {
      return <Redirect to="/login"/>;
    }
    return (
      <div>
        <h2>Become one of our Users! It will be great!</h2>
          {this.renderError()}
          <label>Email:</label>
          <input
            type="email"
            name="email"
            onChange={boundHandleChange}
          />
          <br />
          <label>Password:</label>
          <input
            type="password"
            name="password"
            onChange={boundHandleChange}
          />
          <br />
          <button onClick={boundOnLoginClicked}>Submit!</button>
      </div>
    );
  }
  private renderError() {
    if (this.state.error) {
      return (
        <div className="Login-Error">
          {this.state.error}
        </div>
      );
    }
    return;
  }
  private onRegisterClicked (e:React.MouseEvent<HTMLButtonElement>) {
    fetch(
      '/api/auth/register',
      {
        body: JSON.stringify({
          email: this.state.email,
          password: this.state.password,
        }),
        headers: {
          'content-type': 'application/json',
        },
        method: 'POST',
      },
    )
    .then(
      res => res.json(),
    )
    .then(
      (json) => {
        if (json.error) {
          this.setState({ error: json.error });
        } else {
          this.setState({ done: true });
        }
      },
    );
  }
  private handleChange(e:React.ChangeEvent<HTMLInputElement>) {
    const name = e.target.name;
    const delta = {};
    delta[name] = e.target.value;
    this.setState(delta);
  }

}
