import * as React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import API from '../API'

interface IState {
    done: boolean,
    error: boolean
}

export default class Validate extends React.Component <RouteComponentProps<any>, IState> {
  constructor (props:RouteComponentProps<any>) {
    super(props)

    this.state = {
      done: false,
      error: false
    }

    this.doValidate();
  }
  public render() {

    if(this.state.error) {
      return (
        <div>We are sorry, but there was a validation problem.</div>
      )
    }
    else if(this.state.done){
      return <div>Done, redirecting you in a sec...</div>
    }
    return (
        <div>Validating, please wait...</div>
      )
  }

  private doValidate () {
  fetch(`/api/auth/validate/${this.props.match.params.key}`)
    .then(
      res => res.json(),
    )
    .then(
      (json) => {
        if (json.error) {
          this.setState({ error: true, done:false });
        } else {
          API.onToken(json.token);
        }
      },
    );  }
}
