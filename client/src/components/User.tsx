import * as React from 'react';
import { Redirect, RouteComponentProps } from 'react-router-dom';
import API from '../API';

interface IUser {
  email: string;
  logins: [number];
}

interface IState {
  userData?: IUser;
  deleteState: 'safe' | 'primed' | 'deleting' | 'deleted';
}
export default class User extends React.Component <RouteComponentProps<any>, IState> {
  constructor(props:RouteComponentProps<any>) {
    super(props);

    this.state = {
      deleteState: 'safe',
    };

    this.fetchUser();
  }
  public render() {
    const user = this.state.userData;
    if (this.state.deleteState === 'deleting') {
      return (
        <h3>Deleting user, please wait...</h3>
      );
    }
    if (this.state.deleteState === 'deleted') {
      return (
        <Redirect to="/users"/>
      );
    }
    if (user) {
      const boundPrime = this.primeDelete.bind(this);
      const boundUnPrime = this.unPrimeDelete.bind(this);
      return (
        <div>
          <h2>{user.email}</h2>
          <div style={{ border: '1px solid', padding: '5px' }} onMouseLeave={boundUnPrime}>
            <button
              onClick={boundPrime}
              disabled={this.state.deleteState !== 'safe'}
            >
              Delete this user
            </button>
            {this.renderPrimer()}
          </div>
          {this.renderLogins(user.logins)}
        </div>
      );
    }

    return (
      <div>Loading...</div>
    );
  }

  private renderPrimer () {
    const boundDoDelete = this.doDelete.bind(this);
    if (this.state.deleteState === 'primed') {
      return (
        <span>
          Are you sure you want to delete this user?
          <button onClick={boundDoDelete}>I am!</button>
        </span>
      );
    }
    return undefined;
  }

  private renderLogins(logins?:[number]) {

    const renderLogin = (login:number) => (
      <li key={login}>{new Date(login).toLocaleString()}</li>
    );

    if (logins) {
      return (
        <div>
          <h3>This user has logged in at these times</h3>
          <ul>
            {logins.map(renderLogin)}
          </ul>
        </div>
      );
    }
    return <h3>This user has yet to log in.</h3>;
  }
  private fetchUser ():void {
    API.fetchWithToken(
      '/api/users/' + this.props.match.params.id,
    )
    .then(res => res.json())
    .then(this.onUser.bind(this));
  }

  private onUser(userData:IUser) {
    this.setState({ userData });
  }

  private primeDelete () {
    this.setState({ deleteState: 'primed' });
  }

  private unPrimeDelete () {
    this.setState({ deleteState: 'safe' });
  }

  private doDelete() {
    this.setState({ deleteState: 'deleting' });
    API.fetchWithToken(
      '/api/users/' + this.props.match.params.id,
      { method: 'DELETE' },
    )
    .then(this.onDeleted.bind(this));
  }

  private onDeleted () {
    this.setState({ deleteState: 'deleted' });
  }
}
