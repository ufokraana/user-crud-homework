import * as React from 'react';
import API from '../API';

interface IState {
  email: string;
  password: string;
  error: string;
}

export default class Login extends React.Component <any, IState>{
  private static errorTranslations = {
    NO_USER: 'Your email address is not in our system. Consider registering.',
    WRONG_PASSWORD: 'The provided password was wrong. Consider resetting it.',
  };
  constructor (props:any) {
    super(props);

    this.state = {
      email: '',
      error: '',
      password: '',
    };
  }
  public render() {
    const boundHandleChange = this.handleChange.bind(this);
    const boundOnLoginClicked = this.onLoginClicked.bind(this);

    return (
      <div>
      <h2 style={{ textAlign: 'center' }}>Log in and ensure your success!</h2>
    {this.renderError()}
    <label>Email:</label>
          <input
            type="email"
            name="email"
            onChange={boundHandleChange}
          />
          <br />
          <label>Password:</label>
          <input
            type="password"
            name="password"
            onChange={boundHandleChange}
          />
          <br />
          <button onClick={boundOnLoginClicked}>Submit!</button>
      </div >
    );
  }
  private renderError() {
    if (this.state.error in Login.errorTranslations) {
      return (
        <div className="Login-Error">
          {Login.errorTranslations[this.state.error]}
        </div>
      );
    }
    return;
  }
  private onLoginClicked (e:React.MouseEvent<HTMLButtonElement>) {
    this.setState({ error: '' });
    fetch(
    '/api/auth/login', {
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
      }),
      headers: {
        'content-type': 'application/json',
      },
      method: 'POST',
    },
    )
    .then(
      res => res.json(),
    )
    .then(
      (json) => {
        if (json.error) {
          this.setState({ error: json.error });
        } else {
          API.onToken(json.token);
        }
      },
    );
  }
  private handleChange(e:React.ChangeEvent<HTMLInputElement>) {
    const name = e.target.name;
    const delta = {};
    delta[name] = e.target.value;
    this.setState(delta);
  }

}
