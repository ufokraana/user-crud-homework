import * as React from 'react';

import API from './API';
import PrivateRouter from './PrivateRouter';
import PublicRouter from './PublicRouter';

interface IState {
  user?: {[key: string]: any};
}



class App extends React.Component<{}, IState> {

  private static appStyle = {
    border: '2px solid',
    margin: 'auto',
    'text-align': 'center', // weird - textAlign: 'center' gives a linter error
    width: '60%',
  };
  constructor (props: {}) {
    super(props);

    this.state = {
      user: API.loadFromStorage(),
    };

    API.events.on('userAuth', this.onUserAuth.bind(this));
    API.events.on('userLogout', this.onUserLogout.bind(this));
  }
  public render() {
    const user = this.state.user ? this.state.user.email : 'not logged in';
    return (
      <div style={App.appStyle}>
        <div style={{ backgroundColor: 'black', color: 'white' }}>
          <h1 style={{ margin: '0px' }}>User Crud Homework</h1>
          <span>Your #1 solution for managing user crud since 1897</span>
        </div>
        {this.state.user ? <PrivateRouter/> : <PublicRouter/>}
        <div style={{textAlign: 'left'}}>You are: {user}</div>
      </div>
    );
  }
  private onUserAuth (user: object) {
    this.setState({ user });
  }
  private onUserLogout () {
    this.setState({ user: undefined });
  }
}

export default App;
