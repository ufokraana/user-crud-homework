import * as React from 'react';
import {
  BrowserRouter as Router,
  Link,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';

import Login from './components/Login';
import Register from './components/Register';
import Remind from './components/Remind';
import Validate from './components/Validate'

export default class PublicRouter extends React.Component {
  private static linkStyle = {
    border: '1px solid black',
    color: 'black',
    cursor: 'pointer',
    display: 'inline-block',
    margin: '5px 40px',
    padding: '4px',
    textDecoration: 'none',
  };

  public render() {
    return (
      <div>
        <Router>
          <div>
            <Link style={PublicRouter.linkStyle} to="/login">Login</Link>
            <Link style={PublicRouter.linkStyle} to="/register">Register</Link>
            <Link style={PublicRouter.linkStyle} to="/remind">Recover Password</Link>
              <Switch>
                <Route path="/login" component={Login}/>
                <Route path="/register" component={Register}/>
                <Route path="/remind" component={Remind}/>
                <Route path="/validate/:key" component={Validate}/>
                <Redirect from="/" to="/login"/>
              </Switch>
          </div>
        </Router>
      </div>
    );
  }
}
